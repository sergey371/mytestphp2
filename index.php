<!DOCTYPE html>
<html lang="en-ca">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="./mycss.css" type="text/css">
    <title>Index</title>
</head>
<body id="index">

<div class="outer">
    <header>
        <figure>
            <img src="http://placehold.it/450x115/" alt="MyCompany Logo" width="450" height="115">
            <h1>My Home Maintenance Co.</h1>
            <figcaption>
                <p>No home repair job in the Valley area is too big for us.</p>
            </figcaption>
        </figure>
    </header>

<?php include "./mynav.php"; ?>

    <main>
        <div></div>  <!-- Use whatever tags are appropriate for content. -->
    </main>
    <footer>
        <div></div>  <!-- Use whatever tags are appropriate for content. -->
    </footer>
</div>

</body>
</html>
